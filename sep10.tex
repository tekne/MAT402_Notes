\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT402 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 9 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\DeclareMathOperator{\Area}{Area}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Today, we will begin to study the Ceva Theorem. Assume we are given a triangle \(ABC\). Consider three lines \(\ell_A, \ell_B, \ell_C\) crossing through the vertices of this triangle and intersecting at opposite sides. Under what condition do all three lines pass through one point?
\begin{theorem}[Ceva Theorem]
\[\ell_A \cap \ell_B \cap \ell_C \neq \varnothing \iff \frac{a_1b_1c_1}{a_2b_2c_2} = 1\]
where \(a_1, a_2\) is the line \(CB\) (opposite to \(A\)) by \(\ell_A\), and similarly for \(b_1, b_2, c_1, c_2\).
\end{theorem}
Note that, if \(\ell_A \cap \ell_B \cap \ell_C \neq \varnothing\), we have, where \(p\) is the segment of \(\ell_A\) between \(A\) and the intersection point and \(q\) is the rest, that
\[\frac{p}{q} = \frac{b_2}{b_1} + \frac{c_1}{c_2}\]
and similarly for \(\ell_B, \ell_C\).

We will have several proofs of this result. In fact, similar questions can be asked in non-Euclidean geometry. Assume we have a triangle on a sphere: under what conditions will the three lines going through the vertices intersect. In general, almost every theorem in Euclidean geometry has it's correspondant in Non-Euclidean Geometry. This will be covered next semester in MAT482: Advanced Classical Geometry. This used to be one half-year course, but I decided it was too much to take in at once so I split it into two more detailed parts.

Today we will prove Ceva's theorem using an area argument, specifically the area of the component triangles. Assume that we have a triangle \(ABC\). We know that the area of the triangle is equal to
\[\frac{1}{2}BC \cdot h\]
where \(h\) is the height of the triangle, given by the length of the perpendicular raised from the line \(BC\) to \(A\).

We will study much more general formulas for area and volume, but let me show you a very elementary explanation of why this is true. Assume we have a rectangle \(\delta\) with sides \(a\) and \(b\). Then the area of \(\delta\) is obviously \(ab\).

Let us now consider a parallelogram \(\delta_2\) with side \(b\) and height \(h\). My claim is the area of that figure is given by \(hb\).
To show this, we can transform our parallelogram into a rectangle with base \(b\) and height \(h\) by cutting off the triangle on the left and attaching it to the right (or vice versa).

From this, we can easily find the area of a triangle, by merely taking a triangle and reflecting it across one of it's sides to get a parallelogram with double the area. Computing this area and dividng by two gives the desired result.

We will be using this formula to prove Ceva's theorem, specifically, in the form of the following lemma:
\begin{lemma}
Assume I have a triangle with one of the sides subdivided into two pieces \(a_1, a_2\) by a line coming from the opposite vertex, yielding two triangles \(S_1, S_2\). We claim that
\[\frac{\Area S_1}{\Area S_2} = \frac{\frac{1}{2}a_1h}{\frac{1}{2}a_2h} = \frac{a_1}{a_2}\]
\end{lemma}
Now let me add a trivial observation from elementary school algebra:
\[
  \frac{p}{q} = \frac{a}{b} \land \frac{p}{q} = \frac{c}{d} \implies \forall \lambda \in \reals, \frac{p}{q} = \frac{a + \lambda c}{b + \lambda d}
\]
This is obvious, as we can translate the above identities to
\[bp = aq \land dp = cq\]
Hence, \(\lambda dp = \lambda cq\), giving
\[(b + \lambda d)p = (a + \lambda c)q \implies \frac{p}{q} = \frac{b + \lambda d}{a + \lambda c}\]
I just pointed this out because I don't want to interrupt geometrical arguments with such computations.
Now, let's expand the above lemma a bit:
\begin{lemma}
Consider a triangle with a line segment \(\ell\) passing through a vertex dividing one of it's sides into segments \(a_1, a_2\). Pick any point on \(\ell\) and draw lines from the other vertices to it, dividing the triangle into \(S_1, S_1'\) over \(a_1\) and \(S_2, S_2'\) over \(a_2\). We have
\[\frac{\Area S_1}{\Area S_2} = \frac{a_1}{a_2}\]
\end{lemma}
\begin{proof}
By the first lemma,
\[\frac{\Area S_1 + \Area S_1'}{\Area S_2 + \Area S_2'} = \frac{a_1}{a_2}\]
Hence
\[\frac{\Area S_1'}{\Area S_2'} = \frac{a_1}{a_2} \implies \frac{\Area S_1}{\Area S_2} = \frac{a_1}{a_2}\]
via the arithmetic above
\end{proof}
Now we can prove Ceva's theorem:
\begin{proof}
Take the configuration described in the theorem and assume \(\ell_A, \ell_B, \ell_C\) intersect at one point. Consider the three triangles formed by the sides of the triangles and the lines between the vertices and the intersection point of \(\ell_A, \ell_B, \ell_C\). Denote the triangle having side opposite to \(C\) (\(AB\)) \(S_C\), and similarly for \(S_A, S_B\). We have by the above lemma that
\[\frac{\Area S_C}{\Area S_B} = \frac{a_1}{a_2}, \frac{\Area S_A}{\Area S_C} = \frac{b_1}{b_2}, \frac{\Area S_B}{\Area S_A} = \frac{c_1}{c_2}\]
If we multiply all three together, we get by commutativity
\[\frac{\Area A \cdot \Area B \cdot \Area C}{\Area C \cdot \Area A \cdot \Area B} = 1 = \frac{a_1b_1c_1}{a_2b_2c_2}\]
This proves Ceva's theorem in one direction.
We can now use some algebra to get the other direction: if we know \(\frac{a}{b}\) and \(a + b\), we know \(a\) and \(b\), as
\[\frac{a}{b} = x \land a + b = y \implies a = bx \implies bx + b = y \implies b = \frac{y}{x + 1} \land a = y - b\]
So all we need to know is the proportion of two numbers and their sum. We can now use this to prove Ceva's theorem in the opposite direction: assume that \(\frac{a_1b_1c_1}{a_2b_2c_2} = 1\). We want to prove that \(\ell_A, \ell_B, \ell_C\) intersect at one point.

We'll do something a little bit different: assume we have a new line segment \(\ell\) out of \(C\) intersecting \(AB\) and the intersection of \(\ell_A, \ell_B\), dividing it into segments \(c_1', c_2'\). We have
\[c_1 + c_2 = c_1' + c_2' = AB\]
We know that
\[\frac{c_1}{c_2} = \frac{a_2b_2}{a_1b_1}\]
from our given formula. Furthermore, we know by the forward direction of the theorem that
\[\frac{a_1b_1c_1'}{a_2b_2c_2'} = 1 \implies \frac{c_1'}{c_2'} = \frac{a_2b_2}{a_1b_1}\]
Hence, using the algebra above, as \(c_1, c_2\), \(c_1', c_2'\) have the same sum and same proportions, it follows they are equal, and hence we've proved Ceva's theorem in both directions since \(\ell = \ell_C\).
\end{proof}

Very soon, we will study a different proof of this result using the technique of center of mass, which will allow us to prove many other results as well.

Now, let's prove an additional statement: if we define \(p, q\) as above, specifically the lengths of the segments of \(\ell_A\) split at the intersection, what is \(\frac{p}{q}\)?

Consider the triangles
\begin{itemize}
  \item \(S_B\), with sides \(p, \ell_C\) and \(AC\)
  \item \(S_{a_2}\), with sides \(q, a_2\) and \(\ell_C\)
  \item \(S_C\), with sides \(p, \ell_B\) and \(AB\)
  \item \(S_{a_1}\) with sides \(q, a_1, \ell_B\)
\end{itemize}
We have
\[\frac{\Area S_B}{\Area S_{a_2}} = \frac{\Area S_C}{\Area S_{a_1}} = \frac{p}{q}\]
From here, we can use our arithmetic above to get
\[
  \frac{p}{q} = \frac{\Area S_B + \Area S_C}{\Area S_{a_1} + \Area S_{a_2}} = \frac{\Area S_B + \Area S_C}{\Area S_A}
\]

Now let's talk angles: opposite to side \(a_1\), made with \(\ell_A\), we have angle \(\alpha_1\). The rest of angle \(A\) makes \(\alpha_2\). Similarly, angle \(B\) is split into \(\beta_1, \beta_2\) and \(C\) into \(\gamma_1, \gamma_2\).

We can construct the following quotient:
\[Q = \frac{\sin\alpha_1\sin\beta_1\sin\gamma_1}{\sin\alpha_2\sin\beta_2\sin\gamma_2}\]
We claim that \(Q = 1\) if and only if \(\ell_A \cap \ell_B \cap \ell_C = \varnothing\).

There's a funny story about this next statement: assume we have a triangle not on the plane but on a sphere. We will still have great circles going through and splitting the angles, and it turns out this statement holds. The first statement doesn't hold in spherical geometry, but this one does. So these statements are very different: close, but different.

Let's prove it.
\begin{proof}
Assume \(\ell_A \cap \ell_B \cap \ell_C \neq \varnothing\). Draw perpendiculars from \(BC, AC, AB\) to the intersection point, labelled \(h_a, h_b, h_c\). We have
\[\sin\alpha_1 = \frac{h_c}{p}, \sin\alpha_2 = \frac{h_b}{p}\]
So
\[\frac{\sin\alpha_1}{\sin\alpha_2} = \frac{h_c}{h_b}\]
Similarly,
\[\frac{\sin\beta_1}{\sin\beta_2} = \frac{h_a}{h_c}, \frac{\sin\gamma_1}{\sin\gamma_2} = \frac{h_b}{h_a}\]
Hence,
\[\frac{\sin\alpha_1\sin\beta_1\sin\gamma_1}{\sin\alpha_2\sin\beta_2\sin\gamma_2} = \frac{h_ah_bh_c}{h_ah_bh_c} = 1\]
again giving the forward direction.

The backwards direction is left as an exercise.
\end{proof}
Note that the two statements of Ceva's theorem are equivalent in Euclidean geoemtry, though I prefer to explain them in different ways, as the second argument can be generalized to survive in spherical and hyperbolic geometry, whereas the first argument cannot. So the statements are different, but very very related.

We now want to show three classical results as corollaries from Ceva's theorem:
\begin{itemize}

  \item Three bisectors intersect at one point:
  \begin{proof}
  Since in this case \(\alpha_1 = \alpha_2, \beta_1 = \beta_2, \gamma_1 = \gamma_2\), we can use the second form of Ceva's theorem directly.
  \end{proof}

  \item Three medians intersect at one point:
  \begin{proof}
  By definition, we have \(a_1 = a_2, b_1 = b_2, c_1 = c_2\), hence we can use the first form of Ceva's theorem directly.
  \end{proof}

  It's interesting how Ceva found his results 100 years ago, whereas all these results were found 2000 years ago!

  \item Three heights intersect at one point:
  \begin{proof}
  We have
  \[\frac{a_1}{a_2} = \frac{b}{c}\frac{\cos\gamma}{\cos\beta}\]
  and similarly for \(\frac{b_1}{b_2}, \frac{c_1}{c_2}\).
  We can hence again directly use the first form of Ceva's theorem directly.
  \end{proof}

\end{itemize}

\end{document}
